

import 'package:todo_app/data/todo.dart';

abstract class TodoState {}

class TodoStateInitial extends TodoState {}

class TodoStateSuccess extends TodoState {
   List<Todo> todoList;
   int currentIndex;
   TodoStateSuccess({required this.todoList, required this.currentIndex});

   TodoStateSuccess cloneWith({List<Todo>? todoList, int? currentIndex}) {
      return TodoStateSuccess(
          todoList: todoList ?? this.todoList,
          currentIndex: currentIndex ?? this.currentIndex);
   }
}

class TodoStateLoading extends TodoState {}

class TodoStateError extends TodoState {}
