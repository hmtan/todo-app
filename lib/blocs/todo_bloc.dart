
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/constants.dart';
import 'package:todo_app/data/database.dart';
import 'package:todo_app/data/todo.dart';
import '../events/todo_event.dart';
import 'package:todo_app/states/todo_state.dart';

class TodoBloc extends Bloc<TodoEvent, TodoState> {
  TodoBloc({required this.database}): super(TodoStateInitial());

  List<Todo> _allTodoList = <Todo>[];
  List<Todo> _completeTodoList = <Todo>[];
  List<Todo> _incompleteTodoList = <Todo>[];
  final int _limitQuery = 11;

  final DatabaseTodo database;
  int _currentIndex = Constants.ALL_TODO_LIST_INDEX;

  @override
  Stream<TodoState> mapEventToState(TodoEvent event) async* {
    if(event is TodoEventStarted) {
      emit(TodoStateLoading());
      await loadTodoFromDb();
    }
    else if(event is TodoChangeBottomBarEvent) {
      if(_currentIndex != event.index) {
        _currentIndex = event.index;
        await loadTodoFromDb();
      }
    }
    else if (event is TodoChangeItemStatusEvent) {
      event.item.isComplete = !event.item.isComplete;
      bool ok = await database.update(event.item);
      if(event.handleEventSuccessCallBack != null) {
        event.handleEventSuccessCallBack!(ok);
      }
    }
    else if (event is CreateNewTodoEvent) {
      Todo? todo = await database.insert(event.todo);
      if(todo != null) {
       _allTodoList = [todo,..._allTodoList];
       _incompleteTodoList = [todo,..._incompleteTodoList];
       if(event.handleEventSuccessCallBack != null) {
         event.handleEventSuccessCallBack!(true);
       }
      }
      else {
        if(event.handleEventSuccessCallBack != null) {
          event.handleEventSuccessCallBack!(false);
        }
      }
    }
    //call event when scroll to the end page
    else if (event is LoadItemEvent) {
      if(_currentIndex == Constants.INCOMPLETE_TODO_LIST_INDEX) {
        _incompleteTodoList = [..._incompleteTodoList,
          ...await database.select(_incompleteTodoList.length,_currentIndex,_limitQuery)];
      }
      else if (_currentIndex == Constants.COMPLETE_TODO_LIST_INDEX) {
        _completeTodoList = [..._completeTodoList,...await database.select(_completeTodoList.length,_currentIndex,_limitQuery)];
      }
      else {
        _allTodoList = [..._allTodoList,...await database.select(_allTodoList.length,_currentIndex,_limitQuery)];
      }
    }
    _loadStateFromCurrentIndex();
  }


  Future<void> loadTodoFromDb() async {
    _allTodoList = await database.select(0,Constants.ALL_TODO_LIST_INDEX,_limitQuery);
    _completeTodoList = await database.select(0,Constants.COMPLETE_TODO_LIST_INDEX,_limitQuery);
    _incompleteTodoList = await database.select(0,Constants.INCOMPLETE_TODO_LIST_INDEX,_limitQuery);
  }

  void _loadStateFromCurrentIndex() {
    if(_currentIndex == Constants.ALL_TODO_LIST_INDEX) {
      emit(TodoStateSuccess(todoList: _allTodoList, currentIndex: _currentIndex));
    }
    else if (_currentIndex == Constants.COMPLETE_TODO_LIST_INDEX) {
      emit(TodoStateSuccess(todoList: _completeTodoList, currentIndex: _currentIndex));
    }
    else if (_currentIndex == Constants.INCOMPLETE_TODO_LIST_INDEX) {
      emit(TodoStateSuccess(todoList: _incompleteTodoList, currentIndex: _currentIndex));
    }
  }
}
