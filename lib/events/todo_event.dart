
import 'package:todo_app/constants.dart';
import 'package:todo_app/data/todo.dart';

abstract class TodoEvent{}

class TodoEventStarted extends TodoEvent{}

class TodoChangeBottomBarEvent extends TodoEvent {
  int index;
  TodoChangeBottomBarEvent({required this.index});
}

class TodoChangeItemStatusEvent extends TodoEvent {
  Todo item;
  HandleEventSuccessCallBack? handleEventSuccessCallBack;
  TodoChangeItemStatusEvent({required this.item, this.handleEventSuccessCallBack});
}

class CreateNewTodoEvent extends TodoEvent {
  Todo todo;
  HandleEventSuccessCallBack? handleEventSuccessCallBack;
  CreateNewTodoEvent({required this.todo, this.handleEventSuccessCallBack});
}

class LoadItemEvent extends TodoEvent {}
