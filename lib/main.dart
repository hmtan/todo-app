import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/blocs/todo_bloc.dart';
import 'package:todo_app/constants.dart';
import 'package:todo_app/data/database.dart';
import 'package:todo_app/events/todo_event.dart';
import 'package:todo_app/functions.dart';
import 'package:todo_app/states/todo_state.dart';
import 'package:todo_app/ui/create_todo_page.dart';
import 'package:todo_app/ui/main_page.dart';
import 'data/todo.dart';
import 'ui/item_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  DatabaseTodo? db = await startDb('todo.db');
  runApp(MyApp(newBloc: TodoBloc(database: db!)));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key,required this.newBloc}) : super(key: key);
  final TodoBloc? newBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => newBloc!..add(TodoEventStarted()),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const TodoAppHomePage(),
        ),
    );
  }
}

class TodoAppHomePage extends StatefulWidget {
  const TodoAppHomePage({Key? key}) : super(key: key);

  @override
  _TodoAppHomePageState createState() => _TodoAppHomePageState();
}

class _TodoAppHomePageState extends State<TodoAppHomePage> {

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TodoBloc,TodoState>(
      builder: (context,todoState) {
        if(todoState is TodoStateLoading) {
          return const MainPage(todoList: [],currentIndex: 0);
        }
        else if(todoState is TodoStateSuccess) {
          return MainPage(todoList: todoState.todoList,currentIndex: todoState.currentIndex,isLoading: false,);
        }
        return const Scaffold();
      },
    );
  }





}
