


typedef HandleEventSuccessCallBack = Function(bool);

class Constants {


 // bottom bar index
 static const int ALL_TODO_LIST_INDEX = 0;
 static const int COMPLETE_TODO_LIST_INDEX = 1;
 static const int INCOMPLETE_TODO_LIST_INDEX = 2;

 static const String TODO_PATH = "todo.json";

 static const String PATH_IMAGE_ALL_TODO = "assets/icons/todo.png";
 static const String PATH_IMAGE_TODO_COMPLETE = "assets/icons/todo_complete.png";
 static const String PATH_IMAGE_TODO_INCOMPLETE = "assets/icons/todo_incomplete.png";
 static const String PATH_IMAGE_ADD_TODO = "assets/icons/add_todo.png";


 static const double ICON_BOTTOM_WIDTH = 22;
 static const double ICON_BOTTOM_HEIGHT = 22;

 static const String TODO_LIST_TITLE = "Todo list";
 static const String ALL_TODO_LIST_TITLE = "All";
 static const String TODO_COMPLETE_TITLE = "Complete";
 static const String TODO_INCOMPLETE_TITLE = "Incomplete";
 static const String CREATE_NEW_TODO_TITLTE = "New Todo";

 static const String HINT_TITLE = "title";
 static const String HINT_DESCRIPTION = "Description";


 static const String VALIDATE_INPUT_TITLE = "Please input a title";
 static const String VALIDATE_INPUT_DESCRIPTION = "Please input a description";

 static const String BUTTON_TEXT = "Create";

 static const String MESSAGE_CREATE_NEW_TODO_SUCCESS = "Create new Todo successfully";
 static const String MESSAGE_CREATE_NEW_TODO_FAILURE = "Create new Todo Error";
 static const String MESSAGE_UPDATE_TODO_SUCCESS = "Update Todo successfully";
 static const String MESSAGE_UPDATE_TODO_FAILURE = "Update Todo Error";

}
