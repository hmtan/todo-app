import 'package:sqflite/sqflite.dart';
import 'package:todo_app/data/todo.dart';
import 'package:todo_app/constants.dart';

class DatabaseTodo{
  Database db;

  DatabaseTodo({required this.db});

   static Future<Database> startDb(String name) async {
    return await openDatabase(name,version: 1,onCreate: (db,version) async {
      await db.execute(
          'CREATE TABLE Todo (id INTEGER PRIMARY KEY, title TEXT, description TEXT, is_complete INTEGER)');
    });
  }

  Future<Todo?> insert(Todo todo) async {
    try {
      await db.transaction((txn) async {
        int id = await txn.rawInsert(
            'INSERT INTO Todo(title, description, is_complete) VALUES("${todo.title}", "${todo.description}", ${todo.isComplete ? 1 : 0})');
        todo.id = id;
      });
      return todo;
    } catch (e) {
      return null;
    }
  }

  Future<bool> update(Todo todo) async {
    try {
      await db.rawUpdate(
          'UPDATE Todo SET is_complete = ? WHERE id = ?',
          [todo.isComplete ? 1 : 0, todo.id]);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<List<Todo>> select(int offset,int value,int limit) async {
     String query;
     if(value == Constants.INCOMPLETE_TODO_LIST_INDEX) {
       query = 'SELECT * FROM Todo WHERE is_complete = 0 ORDER BY id DESC LIMIT $limit OFFSET $offset';
     }
     else if (value == Constants.COMPLETE_TODO_LIST_INDEX) {
       query = 'SELECT * FROM Todo WHERE is_complete = 1 ORDER BY id DESC LIMIT $limit OFFSET $offset';
     }
     else {
       query = 'SELECT * FROM Todo ORDER BY id DESC LIMIT $limit OFFSET $offset';
     }
     try {
       List data = await db.rawQuery(query);
       return List<Todo>.from(data.map((e) => Todo.fromJson(e))).toList();
     } catch (e) {
       return <Todo>[];
     }
  }



}
