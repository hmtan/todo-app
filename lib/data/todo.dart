
class Todo {
  int id;
  String title;
  String description;
  bool isComplete;

  Todo({
    this.id = -1,
    required this.title,
    required this.description,
    this.isComplete = false,
  });

  factory Todo.fromJson(Map<String,dynamic> json) {
    return Todo(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        // sqlite not support boolean (false = 0 - true = 1)
        isComplete: json["is_complete"] == 0 ? false : true
    );

  }
}

