
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:todo_app/blocs/todo_bloc.dart';
import 'package:todo_app/constants.dart';
import 'package:todo_app/data/todo.dart';
import 'package:todo_app/events/todo_event.dart';
import 'package:todo_app/functions.dart';

class CreateTodoPage extends StatefulWidget {
  const CreateTodoPage({Key? key}) : super(key: key);

  @override
  _CreateTodoPageState createState() => _CreateTodoPageState();
}

class _CreateTodoPageState extends State<CreateTodoPage> {
  bool _autoValidate = false;
  String _inputTitle = "";
  String _inputDescription = "";
  final GlobalKey<FormState> _inputTitleFormKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _inputDesFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double screenWith = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Stack(
              children: [
                const Align(
                  alignment: Alignment.center,
                  child: Text(Constants.CREATE_NEW_TODO_TITLTE, style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold
                  ),),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    onTap: () => Navigator.of(context).pop(),
                    child: const Icon(Icons.close,color: Colors.black54,),
                  )
                ),
              ],
            ),
          ),
          Container(
            height: 50,
            width: screenWith - 40,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: Colors.grey)
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Form(
              key: _inputTitleFormKey,
              autovalidate: _autoValidate,
              child: TextFormField(
                cursorColor: Colors.black,
                cursorHeight: 15,
                textAlign: TextAlign.left,
                textAlignVertical: TextAlignVertical.top,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 13,
                ),
                decoration: const InputDecoration(
                  hintStyle: TextStyle(
                      color: Colors.black54,
                      fontSize: 13
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white54),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white54),
                  ),
                  hintText: Constants.HINT_TITLE,
                  errorStyle: TextStyle(color: Colors.red,fontSize: 10),
                  errorMaxLines: 1,
                  fillColor: Colors.white12,
                ),
                onChanged: (String? value) {
                  _inputTitle = value!;
                },
                validator: (String? v) {
                  if(v!.trim().isEmpty) {
                    return Constants.VALIDATE_INPUT_TITLE;
                  }
                  return null;
                },
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 100,
            width: screenWith - 40,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: Colors.grey)
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Form(
              key: _inputDesFormKey,
              autovalidate: _autoValidate,
              child: TextFormField(
                maxLines: 3,
                minLines: 1,
                cursorColor: Colors.black,
                cursorHeight: 15,
                textAlign: TextAlign.left,
                textAlignVertical: TextAlignVertical.top,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 13,
                ),
                decoration: const InputDecoration(
                  hintStyle: TextStyle(
                      color: Colors.black54,
                      fontSize: 13
                  ),
                  border: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white54),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white54),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white54),
                  ),
                  hintText: Constants.HINT_DESCRIPTION,
                  errorStyle: TextStyle(color: Colors.red,fontSize: 10),
                  fillColor: Colors.white12,
                ),
                onChanged: (String? value) {
                  _inputDescription = value!;
                },
                validator: (String? v) {
                  if(v!.trim().isEmpty) {
                    return Constants.VALIDATE_INPUT_DESCRIPTION;
                  }
                  return null;
                },
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: _handleCreateTodo,
            child: Container(
                height: 40,
                width: screenWith - 40,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                alignment: Alignment.center,
                child: const Text(Constants.BUTTON_TEXT,style: TextStyle(
                    color: Colors.white
                ),)
            ),
          )
        ],
      ),
    );
  }

  void _handleCreateTodo() {
    final FormState _titleForm = _inputTitleFormKey.currentState!;
    final FormState _desForm = _inputDesFormKey.currentState!;
    if (!_titleForm.validate() || !_desForm.validate() ) {
      setState(() {
        _autoValidate = true;
      });
    }
    else {
      context.read<TodoBloc>().add(
          CreateNewTodoEvent(
              todo: Todo(title: _inputTitle,description: _inputDescription),
              handleEventSuccessCallBack: _handleEventCallBack));
      Navigator.of(context).pop();
    }
  }

  void _handleEventCallBack(bool ok) {
    if(ok) {
      showToastMessage(context,Constants.MESSAGE_CREATE_NEW_TODO_SUCCESS);
    }
    else {
      showToastMessage(context,Constants.MESSAGE_CREATE_NEW_TODO_FAILURE);
    }
  }
}
