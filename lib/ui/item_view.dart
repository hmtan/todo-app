

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:todo_app/blocs/todo_bloc.dart';
import 'package:todo_app/constants.dart';
import 'package:todo_app/events/todo_event.dart';
import 'package:todo_app/functions.dart';
import '../data/todo.dart';


class ItemView extends StatefulWidget {
  const ItemView({Key? key,
    required this.todo,
    this.underLine = true}) : super(key: key);

  final Todo todo;
  final bool underLine;

  @override
  _ItemViewState createState() => _ItemViewState();
}

class _ItemViewState extends State<ItemView> {

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      return Colors.blue;
    }
    return Container(
        height: 60,
        width: MediaQuery.of(context).size.width - 40,
        padding: const EdgeInsets.only(left: 20),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width - 90,
                    padding: const EdgeInsets.only(top: 10,right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.todo.title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 14
                        ),),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(widget.todo.description,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 13
                        ),),
                      ],
                    ),
                  ),
                  Expanded(
                      child: Container(
                        padding:  const EdgeInsets.only(right: 20),
                        child: Checkbox(
                            value: widget.todo.isComplete,
                            checkColor: Colors.white,
                            fillColor: MaterialStateProperty.resolveWith(getColor),
                            onChanged: (bool? value) {
                              setState(() {
                                context.read<TodoBloc>().add(TodoChangeItemStatusEvent(
                                    item: widget.todo,
                                    handleEventSuccessCallBack: _handleUpdateStatusCallBack));
                              });
                            }),
                      ))
                ],
              ),
            ),
            widget.underLine ? Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: 0.5,
                color: Colors.grey[300],
              ),
            ) :  Container()
          ],
        )
    );
  }

  void _handleUpdateStatusCallBack(bool ok) {
    if(ok) {
      showToastMessage(context,Constants.MESSAGE_UPDATE_TODO_SUCCESS);
    }
    else {
      showToastMessage(context,Constants.MESSAGE_UPDATE_TODO_FAILURE);
    }
  }
}



