
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:todo_app/blocs/todo_bloc.dart';
import 'package:todo_app/constants.dart';
import 'package:todo_app/data/todo.dart';
import 'package:todo_app/events/todo_event.dart';
import 'package:todo_app/ui/create_todo_page.dart';
import 'package:todo_app/ui/item_view.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key,
    required this.todoList,
    required this.currentIndex,
    this.isLoading = true}) : super(key: key);

  final List<Todo> todoList;
  final int currentIndex;
  final bool isLoading;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if(_scrollController.position.maxScrollExtent == _scrollController.position.pixels) {
        context.read<TodoBloc>().add(LoadItemEvent());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: _buildAppBar(),
        body: widget.isLoading
            ? Center(child: SizedBox(
            height: 50,
            child: Center(
                child: Platform.isAndroid ? const CircularProgressIndicator(
                  strokeWidth: 1,
                ): const CupertinoActivityIndicator(
                  radius: 15,
                ))),)
            : SingleChildScrollView(
          controller: _scrollController,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0)
                ),
                child: _buildTodoList(widget.todoList,widget.currentIndex)
            ),
          ),
        ),
        bottomNavigationBar: _buildBottomBar(widget.currentIndex));
  }

  Widget _buildTodoList(List<Todo> list,int currentIndex) {
    List<Todo> _items;
    if(currentIndex == Constants.COMPLETE_TODO_LIST_INDEX) {
      _items = list.where((element) => element.isComplete).toList();
    }
    else if (currentIndex == Constants.INCOMPLETE_TODO_LIST_INDEX) {
      _items = list.where((element) => !element.isComplete).toList();
    }
    else {
      _items = list;
    }
    return Column(
        children: List<Widget>.from(_items.map((e) {
          if(_items.indexOf(e) == _items.length - 1) {
            return ItemView(todo: e,underLine: false,);
          }
          return ItemView(todo: e);
        }))
    );
  }

  PreferredSizeWidget _buildAppBar() {
    return AppBar(
      centerTitle: true,
      title: const Text(Constants.TODO_LIST_TITLE,
        style: TextStyle(
            color: Colors.black,
            fontSize: 15
        ),),
      backgroundColor: Colors.grey[200],
      actions: [
        IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => Scaffold(
                    backgroundColor: Colors.transparent,
                    body: Center(
                      child: Container(
                          width: MediaQuery.of(context).size.width - 40,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [
                              CreateTodoPage()
                            ],
                          )),
                    ),)
              );
            },
            icon: Image.asset(Constants.PATH_IMAGE_ADD_TODO,
              color: Colors.black,
              height: Constants.ICON_BOTTOM_HEIGHT,
              width: Constants.ICON_BOTTOM_WIDTH,))
      ],
    );
  }

  Widget _buildBottomBar(int currentIndex,) {
    return BottomNavigationBar(
        currentIndex: currentIndex,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        backgroundColor: Colors.white,
        selectedFontSize: 13,
        unselectedFontSize: 13,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          context.read<TodoBloc>().add(TodoChangeBottomBarEvent(index: index));
        },
        items: [
          _buildBottomBarItem(Constants.PATH_IMAGE_ALL_TODO,Constants.ALL_TODO_LIST_TITLE),
          _buildBottomBarItem(Constants.PATH_IMAGE_TODO_COMPLETE,Constants.TODO_COMPLETE_TITLE),
          _buildBottomBarItem(Constants.PATH_IMAGE_TODO_INCOMPLETE,Constants.TODO_INCOMPLETE_TITLE),

        ]);
  }

  BottomNavigationBarItem _buildBottomBarItem(String path,String title) {
    return BottomNavigationBarItem(
        icon: Image.asset(path,
          height: Constants.ICON_BOTTOM_HEIGHT,
          width: Constants.ICON_BOTTOM_WIDTH,color: Colors.black,),
        activeIcon: Image.asset(path,
          height: Constants.ICON_BOTTOM_HEIGHT,
          width: Constants.ICON_BOTTOM_WIDTH,
          color: Colors.blue,),
        title: Padding(
          padding: const EdgeInsets.only(top: 2),
          child: Text(title),
        ));
  }
}
