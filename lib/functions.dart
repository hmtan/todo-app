

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/data/database.dart';


Future<DatabaseTodo?> startDb(String name) async {
  Database db = await DatabaseTodo.startDb(name);
  return DatabaseTodo(db: db);
}

void showToastMessage(BuildContext context,String message) {
  FToast fToast =  FToast();
  fToast.init(context);
  fToast.showToast(
    child: Container(
      height: 40,
      width: MediaQuery.of(context).size.width - 80,
      decoration: const BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
      ),
      alignment: Alignment.center,
      child: Text(message,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: const TextStyle(
            color: Colors.white,
            fontSize: 15
        ),),
    ),
    gravity: ToastGravity.BOTTOM,
    toastDuration: const Duration(seconds: 1),
  );
}
