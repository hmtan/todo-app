
import 'package:flutter_test/flutter_test.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'package:todo_app/blocs/todo_bloc.dart';
import 'package:todo_app/data/database.dart';
import 'package:todo_app/data/todo.dart';
import 'package:todo_app/events/todo_event.dart';
import 'package:todo_app/states/todo_state.dart';
import 'package:todo_app/constants.dart';


void main() {
  group('Todo db test', () {
    sqfliteFfiInit();
    DatabaseTodo? _database;
    TodoBloc? _bloc;
    setUp(() async {
      TestWidgetsFlutterBinding.ensureInitialized();
      Database db = await databaseFactoryFfi.openDatabase("test.db");
      _database = DatabaseTodo(db: db);
      _bloc = TodoBloc(database: _database!);
    });

    tearDown(() {
      _database!.db.close();
    });

    test('Todo from json', () {
      Map<String,dynamic> content = {
        "id": 1,
        "title": "test",
        "description": "test",
        "is_complete": 0};

      Todo todo = Todo.fromJson(content);

      expect(todo.title, "test");
      expect(todo.description, "test");
      expect(todo.isComplete, false);


    });

    test('Test db', () async {
      expect(await _database!.db.getVersion(), 0);
    });

    test("Insert Todo to db", () async {
      Todo todo = Todo(title: "test",description: "test",isComplete: false);
      Todo? result = await _database!.insert(todo);
      expect(result!.id == -1,false);
    });

    test("Update Todo to db", () async {
      Todo todo = Todo(title: "test1",description: "test1",isComplete: false);
      Todo? result = await _database!.insert(todo);
      result!.isComplete = true;
      expect(await _database!.update(result),true);
    });

    test("select Todo from db",() async {
      Todo todo = Todo(title: "test1",description: "test1",isComplete: false);
      await _database!.insert(todo);
      List<Todo> list = await _database!.select(0,0,10);
      expect(list.isNotEmpty, true);
    });

    test('test bloc started', () async {
      _bloc!.add(TodoEventStarted());
      await Future.delayed(const Duration(milliseconds: 0),() {
        expect(_bloc!.state is TodoStateLoading, true);
      });
      await Future.delayed(const Duration(milliseconds: 100),() {
        expect(_bloc!.state is TodoStateSuccess, true);
        expect((_bloc!.state as TodoStateSuccess).todoList.isNotEmpty, true);
        expect((_bloc!.state as TodoStateSuccess).currentIndex, Constants.ALL_TODO_LIST_INDEX);
      });
    });

    test('test bloc event', () async {

      _bloc!.add(TodoChangeBottomBarEvent(index: Constants.COMPLETE_TODO_LIST_INDEX));
      await Future.delayed(const Duration(milliseconds: 100),() {
        List<Todo> _completeList = (_bloc!.state as TodoStateSuccess).todoList;
        List<Todo> _result = _completeList.where((element) => !element.isComplete).toList();
        expect(_result.isEmpty,true);
      });

      _bloc!.add(TodoChangeBottomBarEvent(index: Constants.INCOMPLETE_TODO_LIST_INDEX));
      Todo _inCompleteTodo = await Future.delayed(const Duration(milliseconds: 100),() {
        List<Todo> _incompleteList = (_bloc!.state as TodoStateSuccess).todoList;
        List<Todo> _result = _incompleteList.where((element) => element.isComplete).toList();
        expect(_result.isEmpty,true);
        return _incompleteList[0];
      });

      // when currentIndex is INCOMPLETE_TODO_LIST_INDEX
      _bloc!.add(TodoChangeItemStatusEvent(item: _inCompleteTodo));
      await Future.delayed(const Duration(milliseconds: 100));
      // move to screen Complete
      _bloc!.add(TodoChangeBottomBarEvent(index: Constants.COMPLETE_TODO_LIST_INDEX));
      await Future.delayed(const Duration(milliseconds: 100), () {
        List<Todo> _completeList = (_bloc!.state as TodoStateSuccess).todoList;
        Todo? result = _completeList.firstWhere((element) => element.id == _inCompleteTodo.id);
        expect(result != null, true);
      });

      // create new todo and insert to db
      _bloc!.add(TodoChangeBottomBarEvent(index: Constants.INCOMPLETE_TODO_LIST_INDEX));
      await Future.delayed(const Duration(milliseconds: 100));
      _bloc!.add(CreateNewTodoEvent(todo: Todo(title: "1",description: "1",isComplete: false)));
      await Future.delayed(const Duration(milliseconds: 100),() {
        List<Todo> _incompleteList = (_bloc!.state as TodoStateSuccess).todoList;
        expect(_incompleteList[0].title, "1");
      });


      // insert 10 todo to db
      for(int i = 0; i < 10; i++) {
        await _bloc!.database.insert(Todo(title: "2",description: "2",isComplete: false));
      }
      // test load todo when scroll listView
      _bloc!.add(TodoChangeBottomBarEvent(index: Constants.ALL_TODO_LIST_INDEX));
      List<Todo> _all = await Future.delayed(const Duration(milliseconds: 100), () {
        return (_bloc!.state as TodoStateSuccess).todoList;
      });
      _bloc!.add(LoadItemEvent());
      await Future.delayed(const Duration(milliseconds: 100),() {
        List<Todo> _list = (_bloc!.state as TodoStateSuccess).todoList;
        expect(_list.length - _all.length, 11);
      });

    });
  });
}



