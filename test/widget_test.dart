import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todo_app/data/todo.dart';
import 'package:todo_app/ui/main_page.dart';

void main() {

  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  testWidgets("test widget", (widgetTester) async {
    List<Todo> list = [];
    for(int i = 0; i < 20; i++ ) {
      list.add(Todo(title: "$i",description: "test",isComplete: false));
    }
    await widgetTester.pumpWidget(MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainPage(todoList: list,currentIndex: 0,isLoading: false,),
    ));
    expect(find.byType(SingleChildScrollView), findsOneWidget);
    await widgetTester.pumpAndSettle();
  });
}


